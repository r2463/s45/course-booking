

let coursesData = [
    {
        id: "wdc001",
        name: "Laravel",
        description: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sunt reiciendis, porro unde dolorum placeat saepe, corporis tempora molestiae itaque impedit dolorem magnam officiis voluptates consequatur vitae eos consequuntur similique commodi!",
        price: 25000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python-Django",
        description: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sunt reiciendis, porro unde dolorum placeat saepe, corporis tempora molestiae itaque impedit dolorem magnam officiis voluptates consequatur vitae eos consequuntur similique commodi!",
        price: 35000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java-Springboot",
        description: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sunt reiciendis, porro unde dolorum placeat saepe, corporis tempora molestiae itaque impedit dolorem magnam officiis voluptates consequatur vitae eos consequuntur similique commodi!",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc004",
        name: "Nodejs-ExpressJS",
        description: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sunt reiciendis, porro unde dolorum placeat saepe, corporis tempora molestiae itaque impedit dolorem magnam officiis voluptates consequatur vitae eos consequuntur similique commodi!",
        price: 55000,
        onOffer: false
    },
]

export default coursesData