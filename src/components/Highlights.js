// import { Fragment } from "react"
import {Card, Row, Col} from 'react-bootstrap'




export default function  Highlights(){

    return(
        <Row>
            <Col xs={12} md={4}>
            <Card className='m-5'>
                <Card.Body>
                    <Card.Title>Learn from Home</Card.Title>
                    <Card.Text>
                        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sunt reiciendis, porro unde dolorum placeat saepe, corporis tempora molestiae itaque impedit dolorem magnam officiis voluptates consequatur vitae eos consequuntur similique commodi!
                    </Card.Text>
                </Card.Body>
            </Card>
            </Col>
            <Col xs={12} md={4}>
            <Card className='m-5'>
                <Card.Body>
                    <Card.Title>Study now, Pay Later </Card.Title>
                    <Card.Text>
                        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sunt reiciendis, porro unde dolorum placeat saepe, corporis tempora molestiae itaque impedit dolorem magnam officiis voluptates consequatur vitae eos consequuntur similique commodi!
                    </Card.Text>
                </Card.Body>
            </Card>   
            </Col>
            <Col xs={12} md={4}>
            <Card className='m-5'>
                <Card.Body>
                    <Card.Title>Be Part of Our Community</Card.Title>
                    <Card.Text>
                        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sunt reiciendis, porro unde dolorum placeat saepe, corporis tempora molestiae itaque impedit dolorem magnam officiis voluptates consequatur vitae eos consequuntur similique commodi!
                    </Card.Text>
                </Card.Body>
            </Card>   
            </Col>
        </Row>
    )


}