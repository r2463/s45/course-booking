import{useState, useEffect} from 'react'
import{Card,Button} from 'react-bootstrap'
import { Link } from 'react-router-dom'


export default function CourseCard({courseProp}){

  let [count, setCount] = useState(0)
 

  const {courseName, description, price, _id} = courseProp
  // console.log(name)
  // console.log(description)
  // console.log(price)


  useEffect (() => {
    console.log('render')
  }, [count])


  const handleClick = () =>{

    if(count < 30){

      setCount(count + 1)
    }
    else{
      alert(`No more seats`)
    }
 
  

  }
    return(
        <Card className="m-5">
          <Card.Body>
            <Card.Title>{courseName}</Card.Title>
              <Card.Subtitle>Description</Card.Subtitle>
              <Card.Text>
                {description}
              </Card.Text>
              <Card.Subtitle>Price</Card.Subtitle>
              <Card.Text>
                {price}
              </Card.Text>
              <Link to={`/courses/${_id}`}>Check Course</Link>
              {/* <Card.Text>{count} Enrollees</Card.Text> */}
              {/* <Button className='btn-info' onClick={handleClick}>Enroll</Button> */}
          </Card.Body>
        </Card>
    )
}