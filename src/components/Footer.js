


export default function Footer(){

    return(
    <div className="bg-info text-white d-flex justify-content-center align-items-center fixed-bottom" style={{height: '10vh'}}>
    <p className='m-0 font-weight-bold'>Roxanne &#64; Course Booking | Zuitt Coding Bootcap &#169;</p>
  </div>
  )
}