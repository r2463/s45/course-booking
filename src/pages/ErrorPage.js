
import Banner from '../components/Banner'


export default function ErrorPage(){

    
  const data = {

    title: "Error 404",
    description: "Page not Found",
    destination: "/",
    buttonDesc: "Go back to Homepage"
  }
  

  return <Banner bannerProp={data}/>
    
    
}