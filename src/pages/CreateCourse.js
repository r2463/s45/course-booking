

import { Container, Row, Col, Form, Button } from 'react-bootstrap'

export default function CreateCourse(){

	return(
		<Container className="container m-5">
		 	<h1 className="text-center">Add Course</h1>
			<Form>
				<Row>
					<Col xs={12} md={8}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Course Name"
					    		type="text" 
					    		// value={}
					    		
					    	/>
						</Form.Group>
					</Col>
					<Col xs={12}  md={4}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		type="number" 
					    		// value={}
					    		
					    	/>
						</Form.Group>
					</Col>
				</Row>
				<Row>
					<Col xs={12}  md={12}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Course Description"
					    		type="text" 
					    		// value={}
					    		
					    	/>
						</Form.Group>
					</Col>
				</Row>
				<Button className="btn btn-info btn-block">Add Course</Button>
			</Form>
		</Container>
	)
}