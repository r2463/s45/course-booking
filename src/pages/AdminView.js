import { useEffect, useState, Fragment, useContext } from 'react'
import { Container, Table, Button } from 'react-bootstrap'
import UserContext from './../UserContext'

export default function AdminView() {

	const [allCourses, setAllCourses] = useState([])

	const { dispatch } = useContext(UserContext)

	const fetchData = () => {
		fetch(`http://localhost:3009/api/courses`, {
			method: "GET",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			dispatch({type: "USER", payload: true})

			setAllCourses( response.map(course => {
				// console.log(course)

				return(
					<tr key={course._id}>
						<td>{course._id}</td>
						<td>{course.courseName}</td>
						<td>{course.price}</td>
						<td>{course.isOffered ? "Active" : "Inactive"}</td>
						<td>
							{
								course.isOffered ?
									<Button 
										className="btn btn-danger mx-2"
										onClick={ () => handleArchive(course._id) }
									>
										Archive
									</Button>
								:
									<Fragment>
										<Button 
											className="btn btn-success mx-2"
											onClick={ () => handleUnarchive(course._id)}
										>
												Unarchive
										</Button>
										<Button 
											className="btn btn-secondary mx-2"
											onClick={ () => handleDelete(course._id) }
										>
											Delete
										</Button>
									</Fragment>
							}
						</td>
					</tr>
				)
			}))
		})
	}

	useEffect(() => {
		fetchData()

	}, [])

	const handleArchive = (courseId) =>{
		console.log(courseId)
		fetch(`http://localhost:3009/api/courses/${courseId}/archive`, {
			method: "PATCH",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()

				alert('Course successfully archived!')
			}
		})
	}

	const handleUnarchive = (courseId) =>{
		console.log(courseId)
		fetch(`http://localhost:3009/api/courses/${courseId}/unarchive`, {
			method: "PATCH",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()
				
				alert('Course successfully Unarchived!')
			}
		})
	}

	const handleDelete = (courseId) =>{
		console.log(courseId)
		fetch(`http://localhost:3009/api/courses/${courseId}/delete-course`, {
			method: "DELETE",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()
				
				alert('Course successfully Deleted!')
			}
		})
	}

	return(
		<Container className="container">
			<h1 className="my-5 text-center">Admin Dashboard</h1>
			<Table>
				<thead>
					<tr>
						<th>ID</th>
						<th>Course Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{ allCourses }
				</tbody>
			</Table>
		</Container>
	)
}
