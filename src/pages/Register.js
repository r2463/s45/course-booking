import {useState, useEffect} from 'react'
import {Form, Button, Row, Col, Container} from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'


export default function Register(){
    
    const [fN, setFN] = useState("")
    const [lN, setlN] = useState("")
    const [email, setEmail] = useState("")
    const [pw, setpw] = useState("")
    const [vpw, setVpw] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)
    
    const navigate = useNavigate();
    
    // useEffect(function, options)
    useEffect (() => {
        console.log('render')

        // if all fields are filled out and pw & vpw is equal, change the state to false
        if(fN !== "" && lN !== "" && email !=="" && pw !=="" && vpw !=="" ){

            setIsDisabled(false)

        }   
        else{
            setIsDisabled(true)

        }

        //listen to state changes: fn, ln, em, pw, vf
    }, [fN, lN, email, pw, vpw])


    const registerUser = (e) => {
        e.preventDefault()
        
        fetch('https://localhost:3009/api/users/email-exist', {

        method: "POST",
        headers: {
            "Content-Type": "application/json"

        },
        body: JSON.stringify({
            email: email
        })
        })
        .then(response => response.json())
        .then(response => {

            // console.log(response)	//false
            if(!response){

                //send request to register
                fetch('https://localhost:3009/api/users/register', {

                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
            
                    },
                    body: JSON.stringify({
                        
                        firstName: fN,
                        lastName: lN,
                        email: email,
                        password: pw
                    })
                    })
                    .then(response => response.json())
                    .then(response => {

                        // console.log(response)
                        if(response){
                            alert(`Registration Successfull`)

                            //redirect
                            navigate('./login')
                        }
                        else {
                            alert(`Something went wrong. Please try again`)
                        }
                    })
            }
                
            else{
                alert(`User already exists`)
            }
        })
    }



    return(
        <Container className='m-5'>
            <h3 className='text-center'>Register</h3>
        <Row className="justify-content-center text-center">
            <Col xs={12} md={6}>
                <Form onSubmit={(e) => registerUser(e)}>
                    <Form.Group className="mb-3">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" value={fN} onChange={(e) => setFN(e.target.value)}/>
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control type="text" value={lN} onChange={(e) => setlN(e.target.value)}/>
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={pw} onChange={(e) => setpw(e.target.value)}/>
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Verify Password</Form.Label>
                        <Form.Control type="password" value={vpw} onChange={(e) => setVpw(e.target.value)}/>
                    </Form.Group>


                    <Button variant="info" type="submit" disabled ={isDisabled}>Submit</Button>
                </Form>
            </Col>
        </Row>
        </Container>
    )
}