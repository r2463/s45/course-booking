import { useReducer } from 'react'
import { BrowserRouter, Routes, Route} from 'react-router-dom';

import {UserProvider} from './UserContext'
import { initialState, reducer } from './reducer/userReducer';


import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import SingleCourse from './pages/SingleCourse';
import CreateCourse from './pages/CreateCourse';
import ErrorPage from './pages/ErrorPage'

import AppNavbar from './components/AppNavbar'
import Footer from './components/Footer'




function App() {
  
  const [state, dispatch] = useReducer(reducer, initialState)
  console.log(state)

  
  return (

    <UserProvider value={{state, dispatch}}>
      <BrowserRouter>
        <AppNavbar/>
          <Routes>
            <Route path="/" element={ <Home/> }/>
            <Route path="/courses" element={ <Courses/> } />
            <Route path="/courses/:courseId" element={ <SingleCourse/> } />
            <Route path="/addCourse" element={ <CreateCourse/> } />
            <Route path="/register" element={ <Register/> } />
            <Route path="/login" element={ <Login/> } />
            <Route path="/logout" element={ <Logout/> } />
            <Route path="*" element={ <ErrorPage/> } />
          </Routes>
        <Footer/>
      </BrowserRouter>
    </UserProvider>

  )
}

export default App;
